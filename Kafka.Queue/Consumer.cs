﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Kafka.Queue.Helper;

namespace Kafka.Queue
{
    public class Consumer : BackgroundService
    {
        private const string KafkaEndpoint = "localhost:9092";
        private const string KafkaGroupId = "banana-consumers";
        private const string KafkaTopic = "banana-topic";
        private readonly IConsumer<Null, string> _consumer;

        //ESTA COMENTADO PORQ ROMPE AL INYECTARLE EL CONFIG
        //public ConsumerService(IOptionsSnapshot<KafkaConfig> kafkaConfig)
        //{
        //    var _kafkaConfig = kafkaConfig.Value ?? throw new ArgumentNullException(nameof(kafkaConfig));

        //    var configEater = new Dictionary<string, string>
        //    {
        //        {"group.id", _kafkaConfig.KafkaGroupId},
        //        {"bootstrap.servers", _kafkaConfig.KafkaEndpoint},
        //        {"enable.auto.commit", "false"}
        //    };
        //    _consumer = new ConsumerBuilder<Null, string>(configEater).Build();

        //    _consumer.Subscribe(_kafkaConfig.KafkaTopic);
        //}

        public Consumer()
        {
            var configEater = new Dictionary<string, string>
            {
                {"group.id", KafkaGroupId},
                {"bootstrap.servers", KafkaEndpoint},
                {"enable.auto.commit", "false"}
            };
            _consumer = new ConsumerBuilder<Null, string>(configEater).Build();

            _consumer.Subscribe(KafkaTopic);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var cr = _consumer.Consume();
                    await HandleAsync(cr.Value);
                    Console.WriteLine($"Se consume el mensaje '{cr.Value}' del topic: '{cr.TopicPartitionOffset}'.");
                    await Task.CompletedTask;
                }
                catch (ConsumeException e)
                {
                    Console.WriteLine($"Error occured: {e.Error.Reason}");
                }
            }
            _consumer.Unsubscribe();
        }

        protected virtual Task HandleAsync(string @event)
        {
            throw new NotImplementedException();
        }
    }
}
