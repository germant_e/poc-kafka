﻿using System;
using Kafka.Abstractions;
using Kafka.Abstractions.DTO;
using System.Threading.Tasks;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace Kafka.Queue
{
    public class Producer : IProducer
    {
        private readonly string _kafkaEndpoint;

        public Producer(
            string kafkaEndpoint)
        {
            _kafkaEndpoint = kafkaEndpoint ?? throw new ArgumentNullException(nameof(kafkaEndpoint));
        }

        public Task PublishAsync(string kafkaTopic, BaseEvent @event)
            => Publish(_kafkaEndpoint, kafkaTopic, @event);

        internal Task Publish(string kafkaEndpoint, string kafkaTopic, object message)
        {
            var config = new ProducerConfig { BootstrapServers = kafkaEndpoint };

            Action<DeliveryReport<Null, string>> handler = r =>
                Console.WriteLine(!r.Error.IsError
                    ? $"Delivered message to {r.TopicPartitionOffset}"
                    : $"Delivery Error:  {r.Error.Reason}");

            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                var produceResult = producer.ProduceAsync(kafkaTopic, new Message<Null, string> { Value = JsonConvert.SerializeObject(message) });
                producer.Flush(TimeSpan.FromSeconds(10));

                return produceResult;
            }
        }
    }
}
