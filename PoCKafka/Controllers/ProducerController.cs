﻿using System;
using System.Threading.Tasks;
using Kafka.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PoCKafka.Config;
using PoCKafka.Events;

namespace PoCKafka.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProducerController : Controller
    {
        private readonly IProducer _producer;
        private readonly KafkaConfig _kafkaConfig;

        public ProducerController(IProducer producer, IOptionsSnapshot<KafkaConfig> kafkaConfig)
        {
            _producer = producer ?? throw new ArgumentNullException(nameof(producer));
            _kafkaConfig = kafkaConfig.Value ?? throw new ArgumentNullException(nameof(kafkaConfig));
        }

        [HttpPost]
        [Route("publicar")]
        public async Task PublicarEvento([FromBody]object evento)
            => await _producer.PublishAsync("FooTopic", new FooEvent(evento));

        //Get rapido para prueba
        [HttpGet]
        [Route("{texto}")]
        public async Task PublicarEvento([FromRoute]string texto)
            => await _producer.PublishAsync(_kafkaConfig.KafkaTopic, new FooEvent(texto));
    }
}