﻿using Kafka.Abstractions.DTO;

namespace PoCKafka.Events
{
    public class FooEvent : BaseEvent
    {
        private readonly object _evento;

        public FooEvent(object evento)
        {
            _evento = evento;
        }
    }
}
