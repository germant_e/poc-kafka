﻿using System;
using System.Threading.Tasks;
using Kafka.Abstractions;
using Kafka.Abstractions.DTO;
using Kafka.Queue;

namespace PoCKafka.EventHandler
{
    public class ConsumerEventHandler : Consumer, IEventHandler<BaseEvent>
    {
        public async Task HandleAsync(BaseEvent @event)
        {
            Console.WriteLine($"Se consume el mensaje '{@event}'.");
            await Task.CompletedTask;
        }
    }
}
