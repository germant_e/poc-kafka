﻿namespace PoCKafka.Config
{
    public class KafkaConfig
    {
        public string KafkaEndpoint { get; set; }
        public string KafkaGroupId { get; set; }
        public string KafkaTopic { get; set; }
    }
}
