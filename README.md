# PoC Kafka
PoC de Kafka .NetCore 2.2

## Instanciar un kafka localmente

La carpeta kafka_2.12-2.3.0 contiene todos los sh y configs necesarios.

Abrir GitBash parado en kafka_2.12-2.3.0.

*  1° starteo el servicio de zookeeper
	-> **bin/zookeeper-server-start.sh config/zookeeper.properties**

*  2° starteo el servicio de kafka
	-> **bin/kafka-server-start.sh config/server.properties**

Listo, ya hay una instancia de kafka funcionando localmente!!!!



Test:
*  Para generar un producer por consola con el topic "test"
	**bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test**

*  Para testear el consumer de la cola "test"
	**bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test** 
	(se puede agregar "--from-beginning \" para ver todos los eventos del topic)