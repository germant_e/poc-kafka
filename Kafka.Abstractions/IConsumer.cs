﻿using Kafka.Abstractions.DTO;

namespace Kafka.Abstractions
{
    public interface IConsumer
    {
        void Subscribe<T>() where T : BaseEvent;
        void Unsubscribe<T>() where T : BaseEvent;
    }
}
