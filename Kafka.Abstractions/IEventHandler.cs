﻿using System.Threading.Tasks;
using Kafka.Abstractions.DTO;

namespace Kafka.Abstractions
{
    public interface IEventHandler<in TEvent> where TEvent : BaseEvent
    {
        Task HandleAsync(TEvent @event);
    }
}
