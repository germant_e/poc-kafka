﻿using Kafka.Abstractions.DTO;
using System.Threading.Tasks;

namespace Kafka.Abstractions
{
    public interface IProducer
    {
        Task PublishAsync(string kafkaTopic, BaseEvent @event);
    }
}
